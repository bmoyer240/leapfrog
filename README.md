|
|:-:
|![leapfrog-logo.png](https://bitbucket.org/repo/6b4rbb/images/1833174038-leapfrog-logo.png)
|![Release Alpha](https://img.shields.io/badge/Release-alpha-yellow.svg?style=flat-square)
|Leapfrog is an extension for **Sublime Text 3** which was developed to improve the flow of work when using packages such as auto-close. Time and time again I found my flow became disrupted when having stretch over to the arrow keys so I could advance the cursor past the closing tag. Now with Leapfrog, users can define a list of characters or character sets to leap over when the leapfrog command is called.
|**For Example** leapfrog is bound to the 'tab' key, because it feels natural, and when a special character is recognized, leapfrog advances the cursor past any closing characters.
|**What about overwriting the default 'tab' commands??** When binding to a key, leapfrog accepts the standard command and argument list, which will be executed if leapfrog fails to find a match.