import sublime

class Config( object ):

  # @var bool whether or not debug is active
  _debug    = False

  # @var obj Settings - settings obj from sublime's api
  _settings = None

  """
   __init__
   calls to load the config file into the sublime
   master object if a filename is given.

   @param  str/list  file  name of the file to load w/o extension

   @return void
  """
  def __init__( self, file = None ):
    if file: self.load( file )
  #}

  """
   load()
   recursive method that loads each file
   into the global sublime setting object.

   @param  str/list  a single filename or list of filenames

   @return void
  """
  def load( self, file ):

    # if a list of strings is passed, attempt to
    # load each file
    if ( isinstance( file, list ) ):
      for f in file: self.load( f )
    #}

    # construct the fullname of the file -
    # the API handles the path
    file += ".sublime-settings"

    self._settings = sublime.load_settings( file )

    if ( self._settings ):
      self._debug = self.get( "debug", False )
  #}

  """
   debug()
   returns the state of the debug option

   @returns bool
  """
  def debug( self ):
    return self._debug
  #}

  """
   get()
   returns the value of the key provided if the
   key exists. A default value can also be passed
   and returned if the key does not exist in the config.

   @param  str    key      name of the setting key
   @param  mixed  default  value to return if key does not exist
  """
  def get( self, key, default = None ):
    return self._settings.get( key, default )
  #}

  """
   set()
   sets the key - value pair in the global settings
   obj.

   @param  str   key
   @param  mixed value

   @return void
  """
  def set( self, key, value ):
      self._settings.set( key, value )
  #}