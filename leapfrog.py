import sublime
import sublime_plugin

from .classes.config   import Config
from .classes.cursor   import Cursor
from .classes.leaplist import LeapList


package = ({
  "author"      : "brian <bmoyer240@gmail.com>",
  "copyright"   : "2015-2016 brian moyer",
  "description" : "Leap over closing characters without having to touch an arrow key.",
  "license"     : "MIT",
  "title"       : "leapfrog",
  "url"         : "",
  "version"     : "1.0.0"
})


class leapfrogCommand( sublime_plugin.TextCommand ):
  # @var obj Config
  _config = None

  # @var obj Cursor
  _cursor = None

  """
   run()
   entry point for all extensions - controls
   the flow of operation.

   @param  obj   *Edit    required param when using TextCommand Class
   @param  str   command  name of an alternative command to exe
   @param  dict  args     args passed into the command call

   @return void
  """
  def run( self, edit, command = None, args = [] ):
    # load the settings
    config = self._config = Config("leapfrog")

    # overwrite the command setting if passed in through keybinding
    if ( command != None ):
      config.set( "command",      command )
      config.set( "command_args", args )

    cursor = self._cursor = Cursor( self.view )

    # exit if a selection is made or cursor is at the eol
    if ( cursor.is_selection() or cursor.at_endOfLine() ):
      return self.teardown()

    # update the cursor
    if ( not cursor.move( self.find_frog() ) ):
      return self.teardown()

  #}

  """
   find_frog()
   the method name could use a little work; however, the
   point is to compare the list of characters to
   skip from the config to the set number of characters
   ahead of the cursor.

   @return  int/false  position of match/false
  """
  def find_frog( self ):
    config = self._config

    leaplist = LeapList()
    leaplist.add( config.get("leap_characters") )
    leaplist.add( config.get("leap_characters_user") )

    # whitespace settings, which allow X number of
    # whitespace chars to preceed the matching charset
    # to account for personal formatting preference
    sti = config.get("spaces_to_ignore")

    # get string > length, trim left space, count whitespace
    str     = self._cursor.get_substr( leaplist.get_maxlen() + sti )
    str_len = len( str )
    str     = str.lstrip()
    str_ws  = abs( str_len - len( str ) )

    # failure - too many spaces
    if ( str_ws > sti ): return False

    list = leaplist.get_list()

    for item in list:
      item_len = len( item )

      # move on if item len is too long
      if ( item_len > str_len ): continue

      # pass on match
      if ( item == str[:item_len] ):
        return item_len + str_ws

    #} for-loop

    return False
  #}

  """
   teardown()
   this method is called at any point of failure giving the
   user the option to define a default command to be executed
   when there are no characters to 'leap' over.

   @example  (keybinding) 'tab' indent the line if there are
                          no characters to 'leap' over

   @return  void
  """
  def teardown( self ):
    command = self._config.get("command" )
    args    = self._config.get("command_args")

    if command:
      self.view.run_command( command, args )
  #}
