import sublime

class Cursor( object ):

  # @var int - the current begin point of the cusor
  _begin  = None

  # @var int - the current end point of the cusor
  _end    = None

  # @var int - point at which the line ends
  _eol    = None

  # @var obj sublime.Region
  region = None

  # @var obj sublime View - injected when the class obj is created
  view   = None

  """
   __init__()

  """
  def __init__( self, view ):
    self.view = view

    # construct the values for the obj w/ the region first
    self.build( view.sel()[0] )
  #}

  """
   build()
   assigns the values to the properties within this class

   @return void
  """
  def build( self, region ):
    self._begin = region.begin()
    self._end   = region.end()
    self._eol   = self.view.line( region ).end()
    self.region = region
  #}

  """
   move()
   removes the current Region with a new region
   which will move the location of the cursor

   @param  int  steps  number of steps to move the cursor

   @return bool
  """
  def move( self, steps = 0 ):
    if ( not steps ): return False

    begin = self.get_begin() + steps

    # remove the old region from the view
    self.view.sel().subtract( self.get_region() )

    # create the new region
    region = sublime.Region( begin, begin )

    # add the new region to the view
    self.view.sel().add( region )

    # update the default values
    self.build( region )

    return True
  #}

# GETTERS ------------------------------------------------//

  """
   get_begin()

   @return int
  """
  def get_begin( self ): return self._begin

  """
   get_end()

   @return int
  """
  def get_end( self ): return self._end

  """
   get_eol()

   @return int
  """
  def get_endOfLine( self ): return self._eol

  """
   get_region()

   @return obj Region
  """
  def get_region( self ): return self.region

  """
   get_substr()
   wrapper for sublime.view.substr - the end is compared
   to the end of line point, and if there are enough
   characters to satisfy the length request, then a
   string will be returned

   @param  int  length  len of the str to return

   @return str/bool
  """
  def get_substr( self, length = 1 ):
    begin = self.get_begin()
    end   = begin + length
    eol   = self.get_endOfLine()
    diff  = abs( end - eol )

    end =  ( end - diff ) if ( end > eol ) else end

    return self.view.substr( sublime.Region( begin, end ) )
  #}

# LOGICAL METHODS ----------------------------------------//

  """
   at_endOfLine()
   checks if the current position of the cursor
   is at the end of the line

   @return  bool
  """
  def at_endOfLine( self ):
    return self.get_begin() >= self.get_endOfLine()
  #}

  """
   is_selection()
   checks if multiple characters/lines are selected

   @return  bool
  """
  def is_selection( self ):
    return len( self.get_region() ) > 0
  #}

# API ----------------------------------------------------//

  """
   scope_name()
   returns the name of the scope at the position
   of the cursor

   @return  string
  """
  def scope_name( self ):
    return self.view.scope_name( self.get_position() )
  #}

#}