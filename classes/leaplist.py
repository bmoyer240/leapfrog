
class LeapList( object ):
  # @var list - contains the list of unique character sets
  list    = []
  max_len = None

  """
   __init__
  """
  def __init__( self, data = [] ):
    # format the data
    if data:
      self.add( data )
  #}

  """
   add()
   recursivly iterates over a list or dict and
   pushes the constrcuted string into
   the _list container

   @param  (list|dict)|string
   @param  str  prefix  gets added to the beginning
                        of the character string

   @return void
  """
  def add( self, characters, prefix = None ):
    # iterate over the list passing back
    # the value into add
    if ( isinstance( characters, list ) ):
      for item in characters:
        #prepend string
        item = prefix + item if prefix else item
        self.add( item )
      #}
      return

    # iterate over dicts to combine values
    # with the key
    elif( isinstance( characters, dict ) ):
      for key, value in characters.items():
        self.add( value, key )
      #}
      return
    #}

    # each item is grouped by length of string
    self.list.append( characters )

    # remove duplicates
    if not prefix:
      self.list = list( set( self.list ) )
      self.set_maxlen()

  #}

  """
   get_list()
  """
  def get_list( self ):
    return self.list;
  #}

  """
   get_maxlen()
  """
  def get_maxlen( self ):
    return self.max_len
  #}

  """
   in_list()
   checks the string arg against the leaplist of
   characters to find a matching value

   @param   str  string to search for in the list

   @return  bool
  """
  def in_list( self, str = "" ):
    key  = len( str )
    list = self.get_list()

    # items are grouped by string length
    if key in list:
      return ( str in list[ key ] )
    #}

    return False
  #}

  def set_maxlen( self ):
    self.max_len = len( max( self.list, key=len ) )
  #}

#} /leaplist